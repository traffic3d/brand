# Traffic3D brand assets

[![pipeline status](https://gitlab.com/traffic3d/brand/badges/main/pipeline.svg)](https://gitlab.com/traffic3d/brand/-/commits/main)

This repository contains brand assets for [Traffic3D](https://traffic3d.org/).

## Credits

The `traffic-semaphore-silhouette.svg` file was published by OpenClipart and is in the Public Domain.
The original file can be found [here](https://freesvg.org/traffic-semaphore-silhouette-vector-image).

The font used in the logos is [Roboto Condensed (bold)](https://fonts.google.com/specimen/Roboto+Condensed).
